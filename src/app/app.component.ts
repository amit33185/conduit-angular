import { Component, OnInit } from "@angular/core";

import { UserService } from "./core";

import Zipy from "zipy-staging-nextjs";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
})
export class AppComponent implements OnInit {
  constructor(private userService: UserService) {}

  ngOnInit() {
    // Staging
    Zipy.init('af7eed22');
    this.userService.populate();
  }
}
